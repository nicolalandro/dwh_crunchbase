# Provision
* Install VirtualBox
* Install Vagrant

    $ vagrant up

* Aprire da Virtualbox la gui e installare:
  * jdk-8u191
  * pgadmin4-3.4-x86
  * postgresql-9.6.10-2
  * TOS_DI
  * TableauDesktop
  * python-3.6.0-webinstall
Non utilizzato
  * installare python su tableau:

    $ pip install tabpy-server
    $ pip install tabpy-client 
