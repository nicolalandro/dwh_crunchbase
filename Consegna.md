STAGING
* implementare almeno due esempi di conversione dei dati sorgenti (es. campi nulli, cast di valori, uppercase to lowercase e vicersa, etc.)
* almeno due esempi di verifica di consistenza dei dati sorgenti (es. chiavi nulle, duplicate, inesistenti, contenuto dei campi non consistente o non valido, etc.)
* Gestione dello Slow Changing Dimension
BI
* Le Colonne siano nel corretto formato
* Le chiavi e le relazioni (Foreign Keys) tra le tabelle siano consistenti
* Dovrà rappresentare necessariamente tutti i livelli delle gerarchie implicite nella struttura delle tabelle fisiche che le compongono.
* user oriented ovvero utilizzi una terminologia orientata all’utente finale per i nomi delle dimensioni, degli attributi e delle misure
* Due misure calcolate di confronto tra valori (es. scostamento valore attuale vs. target, % profitto su venduto, etc.)
* Una delle seguenti misure calcolate basate su serie storica (time series), implementate tramite Quick Table Calculation
  * Confronto periodo su periodo (es. variazione/differenza di valore di una misura rispetto alla mese o al trimestre o anno precedente)
  * Aggregato dall’inizio del periodo alla data corrente (es. YTD)
* Variazione dell’aggregato lungo l’arco temporale degli ultimi N periodi (es. media mobile calcolata sugli ultimi 30 giorni)
* Un indicatore di performance del processo (KPI) implementato tramite un campo calcolato di tipo logico, in grado di assumere un determinato valore di stato (es. “eccellente”, “buono”, “discreto”, “sufficiente”, etc.) in funzione di una misura di riferimento del processo
* Come ultimo passo lo studente dovrà creare uno o più campi calcolati, di tipo parametrico, per implementare un esempio di “analisi what-if”, ovvero una simulazione interattiva che permetta all’utente di indagare sugli effetti di variazione di scenario ottenuti modificando il/i valore dei parametri che controllano i campi calcolati utilizzati nella visualizzazione dei dati (es. simulazione delle variazioni di profitto di un negozio di e-commerce al variare dei costi di spedizione per categorie di prodotti)
* La Dashboard e le worksheet dovranno visualizzare uno o più filtri che permettano all’utente di filtrare di dati in base alle dimensioni e/o alle misure presenti nelle View.
* La navigazione gerarchica dei dati tramite operazioni di drill-down/dirll-up lungo i livelli gerarchici impostati nel data model
