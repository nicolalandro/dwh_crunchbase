-------------------------------------------------------
-- DWH
-------------------------------------------------------
CREATE SCHEMA "DWH";
SET SCHEMA 'DWH';

-- d_comanies 
CREATE TABLE d_companies (
    companies_key serial PRIMARY KEY, 
    permalink varchar(200),
    name varchar(200),
    homepage_url varchar(300),
    category_list varchar(300),
    market varchar(50), -- tipo di mercato: es. videogiochi o Publicitario, o SW
    status varchar(10), -- acquired, operating, closed
    country_code varchar(3),
    state_code varchar(2),
    region varchar(50),
    city varchar(50)
);

-- f_acquisitions
CREATE TABLE f_acquisitions (
    acquisitions_key serial PRIMARY KEY, 
    price_amount numeric(12,0),
    price_currency_code varchar(3),
    acquired_at date, -- d_times
    acquirer_companies serial REFERENCES d_companies(companies_key), 
    acquired_companies serial REFERENCES d_companies(companies_key)
);

-- f_investments
CREATE TABLE f_investments (
    investments_key serial PRIMARY KEY, 
    raised_amount_usd numeric(12,0),
	funded_at date, -- d_times
    investor_companies serial REFERENCES d_companies(companies_key), 
    founded_companies serial REFERENCES d_companies(companies_key),
	founded_round serial
);

-- d_founded_round
CREATE TABLE d_founded_round (
    founded_round_key serial PRIMARY KEY, 
    funding_round_permalink varchar(300),
    funding_round_type varchar(20),
    funding_round_code char(1),
    funded_at date, -- d_times
	raised_amount_usd numeric(12,0)
);
