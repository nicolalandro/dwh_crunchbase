import pandas as pd
import psycopg2
from tqdm import tqdm

TABLE_NAME = '"OLTP".oltp_companies'
TABLE_COLUMN = 'permalink, "name", homepage_url, category_list, market, status, country_code, state_code, region, city, funding_total_usd, funding_rounds,founded_at, first_funding_at, last_funding_at'
CSV_COLUMN = ['permalink', 'name', 'homepage_url', 'category_list', 'market', 'status', 'country_code', 'state_code',
              'region', 'city', 'funding_total_usd', 'funding_rounds', 'founded_at', 'first_funding_at',
              'last_funding_at']


def populate():
    data = pd.read_csv("../../../crunchbase/companies.csv")
    size = len(data)

    try:
        conn = psycopg2.connect(dbname='Crunchbase', host='localhost', port='5432', user='postgres',
                                password='postgres')
        cur = conn.cursor()
        cur.execute('TRUNCATE %s' % TABLE_NAME)
        conn.commit()

        with tqdm(total=size) as pbar:
            pbar.set_description("Processing %s" % 'oltp companies')
            for i, line in data.iterrows():
                values = []
                for col in CSV_COLUMN:
                    if col == 'funding_total_usd':
                        try:
                            values.append(str(float(line[col].replace(",", ""))))
                        except:
                            values.append('0')
                    elif str(line[col]) == 'nan':
                        values.append('Null')
                    else:
                        escaped_val = str(line[col]).replace("'", '"')
                        values.append("'" + escaped_val + "'")
                separator = ', '
                value_volumn = separator.join(values)
                query = 'INSERT INTO %s (%s) VALUES (%s)' % (TABLE_NAME, TABLE_COLUMN, value_volumn)
                # print(query)
                cur.execute(query)
                conn.commit()
                pbar.update(1)
        cur.close()
        conn.close()
    except Exception as e:
        print("Uh oh, can't connect. Invalid dbname, user or password?")
        print(e)


if __name__ == '__main__':
    populate()
