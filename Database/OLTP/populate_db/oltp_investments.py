import pandas as pd
import psycopg2
from tqdm import tqdm

TABLE_NAME = '"OLTP".oltp_investments'
TABLE_COLUMN = 'company_permalink, company_name, company_category_list, company_market, company_country_code, company_state_code, company_region, company_city, investor_permalink, investor_name, investor_category_list, investor_market, investor_country_code, investor_state_code, investor_region, investor_city, funding_round_permalink, funding_round_type, funding_round_code, funded_at, raised_amount_usd'
CSV_COLUMN = ['company_permalink', 'company_name', 'company_category_list', 'company_market', 'company_country_code',
              'company_state_code', 'company_region', 'company_city', 'investor_permalink', 'investor_name',
              'investor_category_list', 'investor_market', 'investor_country_code', 'investor_state_code',
              'investor_region', 'investor_city', 'funding_round_permalink', 'funding_round_type', 'funding_round_code',
              'funded_at', 'raised_amount_usd']


def populate():
    data = pd.read_csv("../../../crunchbase/investments.csv", low_memory=False)
    size = len(data)

    try:
        conn = psycopg2.connect(dbname='Crunchbase', host='localhost', port='5432', user='postgres',
                                password='postgres')
        cur = conn.cursor()
        cur.execute('TRUNCATE %s' % TABLE_NAME)
        conn.commit()

        with tqdm(total=size) as pbar:
            pbar.set_description("Processing %s" % 'oltp investments')
            for i, line in data.iterrows():
                values = []
                for col in CSV_COLUMN:
                    if col == 'raised_amount_usd':
                        try:
                            values.append(str(float(line[col].replace(",", ""))))
                        except:
                            values.append('0')
                    elif str(line[col]) == 'nan':
                        values.append('Null')
                    else:
                        escaped_val = str(line[col]).replace("'", '"')
                        values.append("'" + escaped_val + "'")
                separator = ', '
                value_volumn = separator.join(values)
                query = 'INSERT INTO %s (%s) VALUES (%s)' % (TABLE_NAME, TABLE_COLUMN, value_volumn)
                # print(query)
                cur.execute(query)
                conn.commit()
                pbar.update(1)
        cur.close()
        conn.close()
    except Exception as e:
        print("Uh oh, can't connect. Invalid dbname, user or password?")
        print(e)


if __name__ == '__main__':
    populate()
