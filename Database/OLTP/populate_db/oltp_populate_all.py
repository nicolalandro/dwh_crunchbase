import oltp_companies
import oltp_acquisitions
import oltp_investments
import oltp_rounds

if __name__ == '__main__':
    oltp_companies.populate()
    oltp_acquisitions.populate()
    oltp_investments.populate()
    oltp_rounds.populate()
