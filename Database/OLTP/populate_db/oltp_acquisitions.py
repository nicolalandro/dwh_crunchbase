import pandas as pd
import psycopg2
from tqdm import tqdm

TABLE_NAME = '"OLTP".oltp_acquisitions'
TABLE_COLUMN = 'company_permalink, company_name, company_category_list, company_market, company_country_code, company_state_code, company_region, company_city, acquirer_permalink, acquirer_name, acquirer_category_list, acquirer_market, acquirer_country_code, acquirer_state_code, acquirer_region, acquirer_city, acquired_at, price_amount, price_currency_code'
CSV_COLUMN = ['company_permalink', 'company_name', 'company_category_list', 'company_market', 'company_country_code',
              'company_state_code', 'company_region', 'company_city', 'acquirer_permalink', 'acquirer_name',
              'acquirer_category_list', 'acquirer_market', 'acquirer_country_code', 'acquirer_state_code',
              'acquirer_region', 'acquirer_city', 'acquired_at', 'price_amount', 'price_currency_code']


def populate():
    data = pd.read_csv("../../../crunchbase/acquisitions.csv")
    size = len(data)

    try:
        conn = psycopg2.connect(dbname='Crunchbase', host='localhost', port='5432', user='postgres',
                                password='postgres')
        cur = conn.cursor()
        cur.execute('TRUNCATE %s' % TABLE_NAME)
        conn.commit()

        with tqdm(total=size) as pbar:
            pbar.set_description("Processing %s" % 'oltp acquisitions')
            for i, line in data.iterrows():
                values = []
                for col in CSV_COLUMN:
                    if col == 'price_amount':
                        try:
                            values.append(str(float(line[col].replace(",", ""))))
                        except:
                            values.append('0')
                    elif str(line[col]) == 'nan':
                        values.append('Null')
                    else:
                        escaped_val = str(line[col]).replace("'", '"')
                        values.append("'" + escaped_val + "'")
                separator = ', '
                value_volumn = separator.join(values)
                query = 'INSERT INTO %s (%s) VALUES (%s)' % (TABLE_NAME, TABLE_COLUMN, value_volumn)
                # print(query)
                cur.execute(query)
                conn.commit()
                pbar.update(1)
        cur.close()
        conn.close()
    except Exception as e:
        print(e)


if __name__ == '__main__':
    populate()
