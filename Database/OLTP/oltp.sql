-------------------------------------------------------
-- OLTP (On-line Transaction Processing) 
-------------------------------------------------------
CREATE SCHEMA "OLTP";
SET SCHEMA 'OLTP';

-- companies
CREATE TABLE oltp_companies (
    permalink varchar(200),
    name varchar(200),
    homepage_url varchar(300),
    category_list varchar(300),
    market varchar(50), -- tipo di mercato: es. videogiochi o Publicitario, o SW
    status varchar(10), -- acquired, operating, closed
    country_code varchar(3),
    state_code varchar(2),
    region varchar(50),
    city varchar(50),
    funding_total_usd numeric(12,0),
    funding_rounds numeric(12,0),
    founded_at date,
    first_funding_at date,
    last_funding_at date
);

-- investments
CREATE TABLE oltp_investments (
    company_permalink varchar(200),
    company_name varchar(200),
    company_category_list varchar(300),
    company_market varchar(50),
    company_country_code varchar(3),
    company_state_code varchar(2),
    company_region varchar(50),
    company_city varchar(50),
    investor_permalink varchar(200),
    investor_name varchar(200),
    investor_category_list varchar(300),
    investor_market varchar(50),
    investor_country_code varchar(3),
    investor_state_code varchar(2),
    investor_region varchar(50),
    investor_city varchar(50),
    funding_round_permalink varchar(200),
    funding_round_type varchar(20),
    funding_round_code char(1),
    funded_at date,
    raised_amount_usd numeric(12,0)
);

-- rounds
CREATE TABLE oltp_rounds (
    company_permalink varchar(200),
    company_name varchar(200),
    company_category_list varchar(300),
    company_market varchar(50),
    company_country_code varchar(3),
    company_state_code varchar(2),
    company_region varchar(50),
    company_city varchar(50),
    funding_round_permalink varchar(300),
    funding_round_type varchar(20),
    funding_round_code char(1),
    funded_at date,
    raised_amount_usd numeric(12,0)
);

-- acquisition
CREATE TABLE oltp_acquisitions (
    company_permalink varchar(200),
    company_name varchar(200),
    company_category_list varchar(300),
    company_market varchar(50),
    company_country_code varchar(3),
    company_state_code varchar(2),
    company_region varchar(50),
    company_city varchar(50),
    acquirer_permalink varchar(200),
    acquirer_name varchar(200),
    acquirer_category_list varchar(300),
    acquirer_market varchar(50),
    acquirer_country_code varchar(3),
    acquirer_state_code varchar(2),
    acquirer_region varchar(50),
    acquirer_city varchar(50),
    acquired_at date,
    price_amount numeric(12,0),
    price_currency_code varchar(3)
);

