import pandas as pd

companies_path = '../../crunchbase/companies.csv'
acquisitions_path = '../../crunchbase/acquisitions.csv'
investments_path = '../../crunchbase/investments.csv'
rounds_path = '../../crunchbase/rounds.csv'

companies = pd.read_csv(companies_path);
acquisitions = pd.read_csv(acquisitions_path);
investments = pd.read_csv(investments_path);
rounds = pd.read_csv(rounds_path);


def calculate_max_size(str_name, df):
    print(str_name)
    keys = df.keys();
    for k in keys:
        try:
            max_len =  int(df[k].str.len().max())
            print('\t'+ k + ' '+ str(max_len))
        except:
            print('\t' + k + ' no_string')

if __name__ == '__main__':
    calculate_max_size('Companies', companies)
    calculate_max_size('Investments', investments)
    calculate_max_size('Rounds', rounds)
    calculate_max_size('Acquisitions', acquisitions)

