-------------------------------------------------------
-- STAGING
-------------------------------------------------------
CREATE SCHEMA "DWHS";
SET SCHEMA 'DWHS';

-- dwhs_d_comanies 
CREATE TABLE dwhs_d_companies (
    permalink varchar(200),
    name varchar(200),
    homepage_url varchar(300),
    category_list varchar(300),
    market varchar(50), -- tipo di mercato: es. videogiochi o Publicitario, o SW
    status varchar(10), -- acquired, operating, closed
    country_code varchar(3),
    state_code varchar(2),
    region varchar(50),
    city varchar(50),
    UPD_FLAG character varying(1)
);

-- error_d_companies
CREATE TABLE errors_d_companies (
    permalink varchar(200),
    name varchar(200),
    homepage_url varchar(300),
    category_list varchar(300),
    market varchar(50), -- tipo di mercato: es. videogiochi o Publicitario, o SW
    status varchar(10), -- acquired, operating, closed
    country_code varchar(3),
    state_code varchar(2),
    region varchar(50),
    city varchar(50)
);

-- dwhs_f_acquisitions
CREATE TABLE dwhs_f_acquisitions (
    price_amount numeric(12,0),
    price_currency_code varchar(3),
    acquired_at date, -- d_times
    acquirer_companies_permalink varchar(200), -- chiave esterna acquirer_companies - dwhs_d_comanies 
    acquired_companies_permalink varchar(200), -- chiave esterna acquired_companies - dwhs_d_comanies
    UPD_FLAG character varying(1)
);

-- errors_f_acquisitions
CREATE TABLE errors_f_acquisitions (
    price_amount numeric(12,0),
    price_currency_code varchar(3),
    acquired_at date, -- d_times
    acquirer_companies_permalink varchar(100), -- chiave esterna acquirer_companies - dwhs_d_comanies 
    acquired_companies_permalink varchar(100) -- chiave esterna acquired_companies - dwhs_d_comanies 
);

-- dwhs_f_investments
CREATE TABLE dwhs_f_investments (
    raised_amount_usd numeric(12,0),
    funding_round_permalink varchar(200), -- chiave esterna su d_founded_rounds
    funded_at date, -- d_times
    investor_companies_permalink varchar(100), -- chiave esterna investor_companies - dwhs_d_comanies 
    founded_companies_permalink varchar(100), -- chiave esterna founded_companies - dwhs_d_comanies
    UPD_FLAG character varying(1) 
);

-- errors_f_investments
CREATE TABLE errors_f_investments (
    raised_amount_usd numeric(12,0),
    funding_round_permalink varchar(200), -- d_founded_round
    funded_at date, -- d_times
    investor_companies_permalink varchar(100), -- chiave esterna investor_companies - dwhs_d_comanies 
    founded_companies_permalink varchar(100) -- chiave esterna founded_companies - dwhs_d_comanies 
);

-- dwhs_d_rounds
CREATE TABLE dwhs_d_rounds (
	raised_amount_usd numeric(12,0),
    funding_round_permalink varchar(200),
    funding_round_type varchar(20),
    funding_round_code char(1),
    funded_at date,
    UPD_FLAG character varying(1) 
);

-- errors_d_rounds
CREATE TABLE errors_d_rounds (
	raised_amount_usd numeric(12,0),
    funding_round_permalink varchar(200),
    funding_round_type varchar(20),
    funding_round_code char(1),
    funded_at date,
    UPD_FLAG character varying(1) 
);
